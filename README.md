# pkgparser

[![PyPI version](https://badge.fury.io/py/pkgparser.svg)](https://pypi.org/project/pkgparser/)

Python package for parsing metadata from classes and functions in a python package.
