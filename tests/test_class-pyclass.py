import os

from pkgparser.pypackage.pyclass import PyClass
from pkgparser.pypackage.pypackage import PyPackage

def test():

    # initialize to get package attributes
    pp = PyPackage(os.environ["TEST_PACKAGE"])

    # initialize
    pc = PyClass(
        class_path=os.environ["TEST_PACKAGE_CLASS"],
        directory_path=pp.directory_path
    )

    # no methods on the class
    # if we made it here the class initialized fine
    assert True
