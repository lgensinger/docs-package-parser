import os

from pkgparser.pypackage.pypackage import PyPackage

def test():

    # initialize
    p = PyPackage(os.environ["TEST_PACKAGE"])

    # the only method on the class is tested during initialization
    # if we made it here the class initialized fine
    assert True
